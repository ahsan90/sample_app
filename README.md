# Ruby on Rails Tutorial sample application

This is a simple web applicaton based on Ruby on Rails framework. This is just for practicing purpose followed by the book of "Learn Web Development with Rails" by Michael Hartl.


## Getting started

To get started with the app, clone the repo and then install the needed
gems:```
$ bundle install --without production
```
Next, migrate the database:
```
$ rails db:migrate
```
Finally, run the test suite to verify that everything is working
correctly:
```
$ rails test
```
If the test suite passes, you'll be ready to run the app in a local
server:
```
$ rails server
```
For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).
